package com.vast.crypto.coinbasetest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExchangeFeeDescription {

	@JsonProperty("maker_fee_rate")
	private String makerFeeRate;
	@JsonProperty("taker_fee_rate")
	private String takerFeeRate;
	@JsonProperty("usd_volume")
	private String usdVolume;
}
