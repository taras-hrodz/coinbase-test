package com.vast.crypto.coinbasetest.model;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import quickfix.fix42.NewOrderSingle;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MarketOrderRequest {

	private LocalDateTime sendTime;
	private String orderId;
	private String symbol;
	private Side side;
	private String quantity;
	private String cashQuantity;
	private String stopPrice;
}
