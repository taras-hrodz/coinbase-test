package com.vast.crypto.coinbasetest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ExchangeProductDescription {

	private String id;
	@JsonProperty("display_name")
	private String displayName;
	@JsonProperty("base_currency")
	private String baseCurrency;
	@JsonProperty("quote_currency")
	private String quoteCurrency;
	@JsonProperty("base_increment")
	private String baseIncrement;
	@JsonProperty("quote_increment")
	private String quoteIncrement;
	@JsonProperty("base_min_size")
	private String baseMinSize;
	@JsonProperty("base_max_size")
	private String baseMaxSize;
	@JsonProperty("min_market_funds")
	private String minMarketFunds;
	@JsonProperty("max_market_funds")
	private String maxMarketFunds;
	private String status;
	@JsonProperty("status_message")
	private String statusMessage;
	@JsonProperty("cancel_only")
	private boolean cancelOnly;
	@JsonProperty("limit_only")
	private boolean limitOnly;
	@JsonProperty("post_only")
	private boolean postOnly;
	@JsonProperty("trading_disabled")
	private boolean tradingDisabled;
}