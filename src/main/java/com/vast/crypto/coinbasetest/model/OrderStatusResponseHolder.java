package com.vast.crypto.coinbasetest.model;

import java.lang.reflect.Field;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import lombok.Builder;
import lombok.Data;
import lombok.SneakyThrows;

@Data
@Builder
public class OrderStatusResponseHolder {

	private ExecutionReport orderStatusResponse;
	private Instant executionTime;
	private List<Difference> differencesWithPrevious;

	private Set<Instant> sameResponseCounter;

	@SneakyThrows
	public void fillDifferences(ExecutionReport previousResponse) {
		if (this.differencesWithPrevious == null) {
			this.differencesWithPrevious = new ArrayList<>();
		}
		for (Field field : ExecutionReport.class.getDeclaredFields()) {
			field.setAccessible(true);
			if (!isEqual(field, orderStatusResponse, previousResponse)) {
				this.differencesWithPrevious.add(Difference.builder()
					.fieldName(field.getName())
					.previousValue(field.get(previousResponse) != null ? field.get(previousResponse).toString() : null)
					.newValue(field.get(this.orderStatusResponse) != null ? field.get(this.orderStatusResponse).toString() : null)
					.build());
			}
			field.setAccessible(false);
		}
	}

	public Set<Instant> getSameResponseCounter() {
		if (sameResponseCounter == null) {
			sameResponseCounter = new LinkedHashSet<>();
		}
		return sameResponseCounter;
	}

	@SneakyThrows
	private boolean isEqual(Field field, Object arg1, Object arg2) {
		Object value1 = field.get(arg1);
		Object value2 = field.get(arg2);
		if (value1 == null && value2 == null) {
			return true;
		} else if (value1 == null ^ value2 == null) {
			return false;
		} else {
			return value1.equals(value2);
		}
	}

	@Data
	@Builder
	public static class Difference {

		private String fieldName;
		private String previousValue;
		private String newValue;
	}
}
