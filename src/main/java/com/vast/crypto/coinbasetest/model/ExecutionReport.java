package com.vast.crypto.coinbasetest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ExecutionReport {

	@JsonProperty("id")
	private String orderId;
	@JsonProperty("size")
	private String quantity;
	@JsonProperty("product_id")
	private String symbol;
	@JsonProperty("stp")
	private String selfTradePrevention;
	private String funds;
	@JsonProperty("specified_funds")
	private String specifiedFunds;
	@JsonProperty("type")
	private String orderType;
	@JsonProperty("post_only")
	private boolean isPostOnly;
	@JsonProperty("created_at")
	private String orderCreationTime;
	@JsonProperty("done_at")
	private String orderFinalizedTime;
	@JsonProperty("done_reason")
	private String orderFinalizedStatus;
	@JsonProperty("fill_fees")
	private String fillFees;
	@JsonProperty("filled_size")
	private String filledSize;
	@JsonProperty("executed_value")
	private String executedValue;
	@JsonProperty("status")
	private String orderStatus;
	@JsonProperty("settled")
	private boolean isSettled;
}
