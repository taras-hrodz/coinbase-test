package com.vast.crypto.coinbasetest.model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class E2EResult {

	private MarketOrderRequest marketOrderRequest;
	private List<OrderStatusResponseHolder> responseHolders;

	public List<OrderStatusResponseHolder> getResponseHolders() {
		if (responseHolders == null) {
			responseHolders = new ArrayList<>();
		}
		return responseHolders;
	}

}