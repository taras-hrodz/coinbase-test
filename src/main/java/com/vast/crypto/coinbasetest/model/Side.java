package com.vast.crypto.coinbasetest.model;

public enum Side {
	BUY('1'), SELL('2');

	private char value;

	Side(char value) {
		this.value = value;
	}

	public static Side valueOf(char value) {
		switch (value) {
			case '1':
				return BUY;
			case '2':
				return SELL;
			default:
				throw new IllegalArgumentException("Side: " + value + " is not supported");
		}
	}

	public char getValue() {
		return value;
	}
}
