package com.vast.crypto.coinbasetest.controller;

import com.vast.crypto.coinbasetest.clients.CoinbasePrivateClient;
import com.vast.crypto.coinbasetest.model.E2EResult;
import com.vast.crypto.coinbasetest.model.MarketOrderRequest;
import com.vast.crypto.coinbasetest.model.Side;
import com.vast.crypto.coinbasetest.service.CoinbaseTradeService;
import com.vast.crypto.coinbasetest.service.E2ERunner;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class E2EController {

	private static final String SYMBOL = "BTC-USD";
	private static final Side[] SIDES = {Side.BUY, Side.SELL};
	private static final String[] QUANTITIES_BUY = {"0.002", "0.0015", "0.00135", "0.007467", "0.003456", "0.00694941"};
	private static final String[] QUANTITIES_SELL = {"12.29", "13.45", "20.20", "230.12", "12"};
	private static final Random random = new Random(System.currentTimeMillis());
	private final CoinbasePrivateClient coinbaseConnectorMarketClient;
	private final CoinbaseTradeService coinbaseTradeService;
	private final ExecutorService executorService = Executors.newFixedThreadPool(2);

	@GetMapping("get")
	public List<E2EResult> getResults(@RequestParam int count) throws ExecutionException, InterruptedException {
		List<Future<E2EResult>> futuresList = new ArrayList<>();
		List<E2EResult> results = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			E2ERunner e2ERunner = new E2ERunner(coinbaseConnectorMarketClient, coinbaseTradeService,
				getRandomMarketOrderRequest());
			futuresList.add(executorService.submit(e2ERunner));
			Thread.sleep(90);
		}
		boolean allFuturesCompleted = false;
		while (!allFuturesCompleted) {
			allFuturesCompleted = true;
			for (Iterator<Future<E2EResult>> iterator = futuresList.iterator(); iterator.hasNext(); ) {
				Future<E2EResult> future = iterator.next();
				if (future.isDone()) {
					results.add(future.get());
					iterator.remove();
				} else {
					allFuturesCompleted = false;
				}
			}
		}
		log.info("Total results: " + results.size());
		return results;
	}

	private MarketOrderRequest getRandomMarketOrderRequest() {
		Side side = SIDES[random.nextInt(2)];
		if (side == Side.BUY) {
			return getMarketOrderRequest(side, QUANTITIES_BUY[random.nextInt(QUANTITIES_BUY.length)]);
		} else {
			return getMarketOrderRequest(side, QUANTITIES_SELL[random.nextInt(QUANTITIES_SELL.length)]);
		}
	}

	private MarketOrderRequest getMarketOrderRequest(Side side, String quantity) {
		UUID uuid = UUID.randomUUID();
		MarketOrderRequest.MarketOrderRequestBuilder marketOrderRequestBuilder = MarketOrderRequest.builder()
			.orderId(uuid.toString())
			.symbol(SYMBOL)
			.side(side);
		if (side == Side.BUY) {
			marketOrderRequestBuilder = marketOrderRequestBuilder.quantity(quantity);
		} else {
			marketOrderRequestBuilder = marketOrderRequestBuilder.cashQuantity(quantity);
		}
		return marketOrderRequestBuilder.build();
	}
}
