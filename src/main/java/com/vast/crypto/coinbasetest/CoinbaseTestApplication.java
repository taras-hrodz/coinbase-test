package com.vast.crypto.coinbasetest;

import com.vast.crypto.coinbasetest.config.CoinbaseConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties({CoinbaseConfig.class})
public class CoinbaseTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoinbaseTestApplication.class, args);
	}

}
