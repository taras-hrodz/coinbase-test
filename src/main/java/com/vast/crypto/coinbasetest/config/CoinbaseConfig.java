package com.vast.crypto.coinbasetest.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "coinbase")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CoinbaseConfig {

	private String apiUrl;
	private String socketHost;
	private String socketPort;
	private Keys apiKeys;

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Keys {

		private String apiKey;
		private String secretKey;
		private String passphrase;
	}
}
