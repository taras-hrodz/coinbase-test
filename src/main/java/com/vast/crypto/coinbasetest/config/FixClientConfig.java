package com.vast.crypto.coinbasetest.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;

@ConfigurationProperties(prefix = "fix")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class FixClientConfig {

	private Resource config;
}
