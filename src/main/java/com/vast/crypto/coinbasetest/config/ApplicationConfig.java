package com.vast.crypto.coinbasetest.config;

import java.io.InputStream;
import java.util.Properties;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import quickfix.FileStoreFactory;
import quickfix.MessageFactory;
import quickfix.MessageStoreFactory;
import quickfix.SLF4JLogFactory;
import quickfix.SessionSettings;

@Configuration
public class ApplicationConfig {

	private static final String FIX_SOCKET_HOST = "FIX_SOCKET_HOST";
	private static final String FIX_SOCKET_PORT = "FIX_SOCKET_PORT";
	private static final String API_KEY = "API_KEY";

	@Bean
	public FixClientConfig fixClientConfig() {
		return new FixClientConfig();
	}

	@Bean
	public CoinbaseConfig coinbaseConfig() {
		return new CoinbaseConfig();
	}

	@Bean
	public SessionSettings fixSessionSettings(FixClientConfig cfg, CoinbaseConfig coinbaseConfig) {
		try (InputStream inputStream = cfg.getConfig().getInputStream()) {
			Properties properties = new Properties();
			properties.put(FIX_SOCKET_HOST, coinbaseConfig.getSocketHost());
			properties.put(FIX_SOCKET_PORT, coinbaseConfig.getSocketPort());
			properties.put(API_KEY, coinbaseConfig.getApiKeys().getApiKey());
			return new SessionSettings(inputStream, properties);
		} catch (Exception ex) {
			throw new BeanInitializationException("Could not initialize FIX SessionSettings", ex);
		}
	}

	@Bean
	public MessageStoreFactory messageStoreFactory(SessionSettings sessionSettings) {
		return new FileStoreFactory(sessionSettings);
	}

	@Bean
	public MessageFactory messageFactory() {
		return new quickfix.fix42.MessageFactory();
	}

	@Bean
	public SLF4JLogFactory slf4JLogFactory(SessionSettings sessionSettings) {
		return new SLF4JLogFactory(sessionSettings);
	}
}
