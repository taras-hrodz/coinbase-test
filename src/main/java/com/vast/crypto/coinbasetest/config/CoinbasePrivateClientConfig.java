package com.vast.crypto.coinbasetest.config;

import com.vast.crypto.coinbasetest.config.CoinbaseConfig.Keys;
import com.vast.crypto.coinbasetest.util.CoinbaseUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CoinbasePrivateClientConfig {

	private final CoinbaseConfig coinbaseConfig;

	@Bean
	public CoinbaseAuthInterceptor coinbaseAuthInterceptor() {
		return new CoinbaseAuthInterceptor();
	}

	public class CoinbaseAuthInterceptor implements RequestInterceptor {

		@Override
		public void apply(RequestTemplate template) {
			final Keys keys = coinbaseConfig.getApiKeys();
			final long timestampInSeconds = System.currentTimeMillis() / 1000;
			final String signature = CoinbaseUtils
				.signHttpMessage(timestampInSeconds, template.method(), template.path(), template.body(),
					keys.getSecretKey());

			template.header("CB-ACCESS-KEY", keys.getApiKey());
			template.header("CB-ACCESS-SIGN", signature);
			template.header("CB-ACCESS-TIMESTAMP", String.valueOf(timestampInSeconds));
			template.header("CB-ACCESS-PASSPHRASE", keys.getPassphrase());
		}
	}

}
