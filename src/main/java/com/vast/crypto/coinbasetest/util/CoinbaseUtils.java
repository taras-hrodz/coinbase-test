package com.vast.crypto.coinbasetest.util;

import java.util.Base64;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import quickfix.Message;
import quickfix.UtcTimestampPrecision;
import quickfix.field.MsgSeqNum;
import quickfix.field.MsgType;
import quickfix.field.SenderCompID;
import quickfix.field.SendingTime;
import quickfix.field.TargetCompID;
import quickfix.field.converter.UtcTimestampConverter;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CoinbaseUtils {

	public static String signFixMessage(Message message, String passphrase, String secret) {
		try {
			SendingTime sendingTime = new SendingTime();
			message.getHeader().getField(sendingTime);

			MsgType msgType = new MsgType();
			message.getHeader().getField(msgType);

			MsgSeqNum msgSeqNum = new MsgSeqNum();
			message.getHeader().getField(msgSeqNum);

			SenderCompID senderCompID = new SenderCompID();
			message.getHeader().getField(senderCompID);

			TargetCompID targetCompID = new TargetCompID();
			message.getHeader().getField(targetCompID);

			String presign =
				UtcTimestampConverter.convert(sendingTime.getValue(), UtcTimestampPrecision.MILLIS)
					+ '\u0001' + msgType.getValue()
					+ '\u0001' + msgSeqNum.getValue() + '\u0001' + senderCompID.getValue()
					+ '\u0001' + targetCompID.getValue()
					+ '\u0001' + passphrase;

			byte[] secretBytes = Base64.getDecoder().decode(secret);
			SecretKeySpec keyspec = new SecretKeySpec(secretBytes, "HmacSHA256");
			Mac sha256 = Mac.getInstance("HmacSHA256");
			sha256.init(keyspec);
			return Base64.getEncoder().encodeToString(sha256.doFinal(presign.getBytes()));
		} catch (Exception ex) {
			throw new RuntimeException("Could not generate signature", ex);
		}
	}

	public static String signHttpMessage(long timeInSeconds, String method, String path,
		byte[] body, String secret) {
		try {
			String presign = timeInSeconds + method + path;

			if (body != null) {
				presign += new String(body);
			}

			byte[] secretBytes = Base64.getDecoder().decode(secret);
			SecretKeySpec keyspec = new SecretKeySpec(secretBytes, "HmacSHA256");
			Mac sha256 = Mac.getInstance("HmacSHA256");
			sha256.init(keyspec);
			return Base64.getEncoder().encodeToString(sha256.doFinal(presign.getBytes()));
		} catch (Exception ex) {
			throw new RuntimeException("Could not generate signature", ex);
		}
	}
}
