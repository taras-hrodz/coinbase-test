package com.vast.crypto.coinbasetest.mapper;

import com.vast.crypto.coinbasetest.model.MarketOrderRequest;
import java.math.BigDecimal;
import quickfix.field.CashOrderQty;
import quickfix.field.ClOrdID;
import quickfix.field.HandlInst;
import quickfix.field.OrdType;
import quickfix.field.OrderQty;
import quickfix.field.Side;
import quickfix.field.StopPx;
import quickfix.field.Symbol;
import quickfix.fix42.NewOrderSingle;

public class CoinbaseMessageMapper {

	public static NewOrderSingle map(MarketOrderRequest entity) {
		NewOrderSingle order = new NewOrderSingle();
		order.set(new Side(entity.getSide().getValue()));
		order.set(new ClOrdID(entity.getOrderId()));
		order.set(new Symbol(entity.getSymbol()));
		order.set(new OrdType(OrdType.MARKET));
		order.set(new HandlInst('1'));
		if (entity.getQuantity() != null) {
			order.set(new OrderQty(new BigDecimal(entity.getQuantity()).doubleValue()));
		}
		if (entity.getCashQuantity() != null) {
			order.set(new CashOrderQty(new BigDecimal(entity.getCashQuantity()).doubleValue()));
		}
		if (entity.getStopPrice() != null) {
			order.set(new StopPx(new BigDecimal(entity.getStopPrice()).doubleValue()));
		}

		return order;
	}
}
