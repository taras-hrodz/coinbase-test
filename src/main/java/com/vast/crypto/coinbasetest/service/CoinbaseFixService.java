package com.vast.crypto.coinbasetest.service;

import com.vast.crypto.coinbasetest.config.CoinbaseConfig;
import com.vast.crypto.coinbasetest.config.CoinbaseConfig.Keys;
import com.vast.crypto.coinbasetest.util.CoinbaseUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import quickfix.Application;
import quickfix.Message;
import quickfix.SessionID;
import quickfix.StringField;
import quickfix.field.EncryptMethod;
import quickfix.field.RawData;
import quickfix.field.RawDataLength;
import quickfix.fix42.Logon;

@Service
@AllArgsConstructor
public class CoinbaseFixService implements Application {

	private static final Logger LOG = LoggerFactory.getLogger(CoinbaseFixService.class);
	private final FixSessionService sessionService;
	private final CoinbaseConfig coinbaseConfig;

	@Override
	public void onCreate(SessionID sessionId) {
		LOG.info("FIX session: " + sessionId + " created.");
	}

	@Override
	public void onLogon(SessionID sessionId) {
		LOG.info("Logon succeed for session: " + sessionId);
		sessionService.addSession(sessionId);
	}

	@Override
	public void onLogout(SessionID sessionId) {
		LOG.info("Logout session: " + sessionId);
		sessionService.removeSession(sessionId);
	}

	@Override
	public void toAdmin(Message message, SessionID sessionId) {
		if (message instanceof Logon) {
			Logon logonMessage = (Logon) message;
			Keys keys = coinbaseConfig.getApiKeys();
			String signature = CoinbaseUtils.signFixMessage(message, keys.getPassphrase(),
				keys.getSecretKey());
			logonMessage.set(new RawData(signature));
			logonMessage.set(new RawDataLength(signature.length()));
			logonMessage.set(new EncryptMethod(0));
			logonMessage.setField(new StringField(554, keys.getPassphrase()));
			logonMessage.setField(new StringField(8013, "S"));
		}
		LOG.info("To Admin message: " + message);
	}

	@Override
	public void fromAdmin(Message message, SessionID sessionId) {
		LOG.info("From Admin message: " + message);
	}

	@Override
	public void toApp(Message message, SessionID sessionId) {
		LOG.info("To App message: " + message);
	}

	@Override
	public void fromApp(Message message, SessionID sessionId) {
		LOG.info("From App message: " + message);
	}
}
