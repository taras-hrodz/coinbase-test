package com.vast.crypto.coinbasetest.service;

import com.vast.crypto.coinbasetest.clients.CoinbasePrivateClient;
import com.vast.crypto.coinbasetest.model.E2EResult;
import com.vast.crypto.coinbasetest.model.ExecutionReport;
import com.vast.crypto.coinbasetest.model.MarketOrderRequest;
import com.vast.crypto.coinbasetest.model.OrderStatusResponseHolder;
import feign.FeignException;
import java.time.Instant;
import java.util.concurrent.Callable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class E2ERunner implements Callable<E2EResult> {

	private final CoinbasePrivateClient coinbaseClient;
	private final CoinbaseTradeService coinbaseTradeService;
	private final MarketOrderRequest marketOrderRequest;

	@Override
	public E2EResult call() throws Exception {
		E2EResult e2eResult = new E2EResult();
		e2eResult.setMarketOrderRequest(marketOrderRequest);
		coinbaseTradeService.placeMarketOrder(marketOrderRequest);
		Thread.sleep(5);
		boolean isFinalized = false;
		int errorCounter = 0;
		while (!isFinalized) {
			ExecutionReport orderStatus;
			try {
				orderStatus = coinbaseClient.getOrderStatus(marketOrderRequest.getOrderId()).getBody();
			} catch (FeignException e) {
				log.error(e.getMessage());
				errorCounter++;
				if (errorCounter < 5) {
					continue;
				} else {
					break;
				}
			}
			log.info(marketOrderRequest.getOrderId() + " - " + orderStatus.getOrderStatus() + " - " + orderStatus.getOrderFinalizedStatus());
			if (e2eResult.getResponseHolders().isEmpty()) {
				e2eResult.getResponseHolders().add(OrderStatusResponseHolder.builder()
					.orderStatusResponse(orderStatus)
					.executionTime(Instant.now())
					.build());
			} else {
				OrderStatusResponseHolder previousResponse =
					e2eResult.getResponseHolders().get(e2eResult.getResponseHolders().size() - 1);
				if (previousResponse.getOrderStatusResponse().equals(orderStatus)) {
					previousResponse.getSameResponseCounter().add(Instant.now());
					if (previousResponse.getOrderStatusResponse().getOrderStatus().equals("done") &&
						previousResponse.getOrderStatusResponse().getOrderFinalizedStatus().equals("filled") &&
						!previousResponse.getSameResponseCounter().isEmpty()) {
						isFinalized = true;
					}
				} else {
					OrderStatusResponseHolder orderStatusResponseHolder =
						OrderStatusResponseHolder.builder()
							.orderStatusResponse(orderStatus)
							.executionTime(Instant.now())
							.build();
					orderStatusResponseHolder.fillDifferences(previousResponse.getOrderStatusResponse());
					e2eResult.getResponseHolders().add(orderStatusResponseHolder);
				}
			}
			Thread.sleep(100);
		}
		return e2eResult;
	}
}
