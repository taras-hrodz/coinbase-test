package com.vast.crypto.coinbasetest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import quickfix.Application;
import quickfix.ConfigError;
import quickfix.MessageFactory;
import quickfix.MessageStoreFactory;
import quickfix.SLF4JLogFactory;
import quickfix.SessionSettings;
import quickfix.SocketInitiator;

@Service
@RequiredArgsConstructor
public class FixConnectionService {

	private final SessionSettings sessionSettings;
	private final MessageFactory messageFactory;
	private final SLF4JLogFactory logFactory;
	private final MessageStoreFactory messageStoreFactory;
	private final Application application;
	private SocketInitiator socket;

	public void start() throws ConfigError {
		socket = new SocketInitiator(
			application,
			messageStoreFactory,
			sessionSettings,
			logFactory,
			messageFactory);
		socket.start();
	}

	public void stop() {
		if (socket != null) {
			socket.stop(true);
		}
	}
}
