package com.vast.crypto.coinbasetest.service;

import com.vast.crypto.coinbasetest.mapper.CoinbaseMessageMapper;
import com.vast.crypto.coinbasetest.model.MarketOrderRequest;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import quickfix.fix42.NewOrderSingle;

@Service
@AllArgsConstructor
@Slf4j
public class CoinbaseTradeService {

	private final FixSessionService sessionService;

	public void placeMarketOrder(MarketOrderRequest order) {
		sessionService.getAnySession().orElseThrow(() -> new RuntimeException("Session not found"))
			.send(CoinbaseMessageMapper.map(order));
		order.setSendTime(LocalDateTime.now());
	}
}
