package com.vast.crypto.coinbasetest.service;

import com.vast.crypto.coinbasetest.config.CoinbaseConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import quickfix.Session;
import quickfix.SessionID;

@Service
@RequiredArgsConstructor
public class FixSessionService {

	private final CoinbaseConfig coinbaseConfig;
	private final Map<SessionID, Session> sessionCache = new HashMap<>();

	public Optional<Session> getAnySession() {
		return sessionCache.values().stream().findAny();
	}

	public void addSession(SessionID sessionID) {
		sessionCache.put(sessionID, Session.lookupSession(sessionID));
	}

	public void removeSession(SessionID sessionID) {
		sessionCache.remove(sessionID);
	}
}
