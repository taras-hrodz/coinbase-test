package com.vast.crypto.coinbasetest.clients;

import com.vast.crypto.coinbasetest.config.CoinbasePrivateClientConfig;
import com.vast.crypto.coinbasetest.model.ExecutionReport;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CoinbasePrivateClient", url = "${coinbase.apiUrl}",
	configuration = CoinbasePrivateClientConfig.class)
public interface CoinbasePrivateClient {

	@GetMapping(value = "/orders/client:{clientOrderId}")
	ResponseEntity<ExecutionReport> getOrderStatus(
		@PathVariable(value = "clientOrderId") String clientOrderId);
}
