package com.vast.crypto.coinbasetest.listener;

import com.vast.crypto.coinbasetest.service.FixConnectionService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import quickfix.ConfigError;

@Component
@AllArgsConstructor
public class ApplicationEventListener {

	private final FixConnectionService connectionService;

	@EventListener
	public void handleStreams(ApplicationReadyEvent event) throws ConfigError {
		connectionService.start();
	}
}
