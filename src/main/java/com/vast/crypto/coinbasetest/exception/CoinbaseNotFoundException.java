package com.vast.crypto.coinbasetest.exception;

public class CoinbaseNotFoundException extends CoinbaseException {

	public CoinbaseNotFoundException(String message) {
		super(message);
	}
}
