package com.vast.crypto.coinbasetest.exception;

public class CoinbaseException extends RuntimeException {

	public CoinbaseException(String message) {
		super(message);
	}
}
